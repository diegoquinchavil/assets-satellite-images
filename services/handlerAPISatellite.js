

class handlerAPISatellite{
    
    constructor(){
        this.url_api = "https://services.sentinel-hub.com/ogc/wms/"; //url sentinel
        this.url_token = "cc557263-de04-431c-9856-65297c9926fe" + "?"; //token
        this.service = "SERVICE=WMS";
        this.request = "REQUEST=GetMap";
        this.version = "";
        this.url_layers = "TRUE_COLOR"; // layers (TRUE_COLOR, NDVI)
        this.maxcc = "20";
        this.gain = "";
        this.gamma = "";
        this.width = "";
        this.height = "";
        this.crs = "EPSG";
        this.format ="image/png"; // image format (png, jpeg)
        this.transparent = "";
        this.time = "";
        this.bounds = "";
        this.polygon = "";
    }
    
    /* getters */
    getUrlApi(){ return this.url_api; }
    getUrlToken(){ return this.url_token; }
    getService(){ return this.service; }
    getRequest(){ return this.request; }
    getVersion(){ return this.version; }
    getUrlLayers(){ return this.url_layers; }
    getMaxCC(){ return this.maxcc; }
    getGain(){ return this.gain; }
    getGamma(){ return this.gamma; }
    getWidth(){ return this.width; }
    getHeight(){ return this.height; }
    getCrs(){ return this.crs; }
    getFormat(){ return this.format; }
    getTransparent(){ return this.transparent; }
    getTime(){ return this.time; }
    getBounds(){ return this.bounds; }
    getPolygon(){ return this.polygon; }
    
    
    /* setters */
    setUrlApi( _urlapi ){ this.url_api = _urlapi; }
    setUrlToken( _urltoken ){ this.url_token = _urltoken; }
    setService( _service ){ this.service = "SERVICE=" + _service; }
    setRequest( _request ){ this.request = "REQUEST=" +_request; }
    setVersion( _version ){ this.version = "VERSION=" + _version; }
    setUrlLayers( _urllayers ){ this.url_layers = "LAYERS=" + _urllayers; }
    setMaxCC( _maxcc ){ this.maxcc = "MAXCC=" + _maxcc; }
    setGain( _gain ){ this.gain = "GAIN=" + _gain; }
    setGamma( _gamma ){ this.gamma = "GAMMA=" + _gamma; }
    setWidth( _width ){ this.width = "WIDTH=" + _width; }
    setHeight( _height ){ this.height = "HEIGHT=" + _height; }
    setCrs( _crs ){ this.crs = "CRS=" + _crs; }
    setFormat( _format ){ this.format = "FORMAT=" + _format; }
    setTransparent( _transparent ){ this.transparent = "TRANSPARENT="+_transparent; }
    setTime( _time ){ this.time = "TIME=" + _time; }
    setBounds( _bounds ){ this.bounds = "BBOX=" + _bounds; }
    setPolygon( _polygon ){ this.polygon = "GEOMETRY=POLYGON((" + _polygon + "))"; }
    
    /* functions */
    getEndpointAPI(){
        let endpoint = this.url_api + this.url_token + "?" +
                       this.service + "&" + this.request + "&" +
                       this.version + "&" + this.url_layers + "&" +
                       this.maxcc + "&" + this.gain + "&" + this.gamma + "&" +
                       this.width + "&" + this.height + "&" +
                       this.crs + "&" + this.format + "&" + this.transparent + "&" +
                       this.time + "&" + this.bounds + "&" + this.polygon;
        
        return endpoint;        
    }
        
}


module.exports = handlerAPISatellite;