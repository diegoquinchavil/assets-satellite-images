const builder = require("xmlbuilder");

class handlerBuildKML{
    
    constructor(){
        //init params for xml-kml
        this.kml = builder.create('kml');
        this.kml.att('xmlns', 'http://www.opengis.net/kml/2.2');
	    this.kml.att('xmlns:gx', 'http://www.google.com/kml/ext/2.2');
	    this.kml.att('xmlns:kml', 'http://www.opengis.net/kml/2.2');
	    this.kml.att('kmlns:atom', 'http://www.w3.org/2005/Atom');
    
        this.groundoverlay = null;
        this.style = null;
    }
    
    addGroundOverlay( _name ){
        this.groundoverlay = this.kml.ele('GroundOverlay');
        this.groundoverlay.ele('name', _name);
    }
    
    addGroundOverlayIcon( _href ){
        let Icon = this.groundoverlay.ele('Icon');
            Icon.ele('href', _href);
            Icon.ele('viewBoundScale', 0.75);
    }
    
    addGroundOverlayBox( bounds ){
        let LatLonBox = this.groundoverlay.ele('LatLonBox');
            LatLonBox.ele('north', bounds.north);
            LatLonBox.ele('south', bounds.south);
            LatLonBox.ele('east', bounds.east);
            LatLonBox.ele('west', bounds.west);
    }
    
    /* functions */
    getKMLString(){
        return '<?xml version="1.0" encoding="UTF-8"?>' + this.kml.toString({ pretty : true });
    }
    
}


module.exports = handlerBuildKML;