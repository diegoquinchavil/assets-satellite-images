const gdal = require("gdal");

class handlerGdal{
    constructor( pathfile ){
        this.dataset = gdal.open( pathfile );
        this.corners = {
            'Upper Left  ' : {x: 0, y: 0},
            'Upper Right ' : {x: this.dataset.rasterSize.x, y: 0},
            'Bottom Right' : {x: this.dataset.rasterSize.x, y: this.dataset.rasterSize.y},
            'Bottom Left ' : {x: 0, y: this.dataset.rasterSize.y},
            'Center      ' : {x: this.dataset.rasterSize.x / 2, y: this.dataset.rasterSize.y / 2}
        };
        this.geotransform = this.dataset.geoTransform;
    }

    getSizeFile(){ return this.dataset.size; }

    getBoundsTiff(){

        let wgs84                   = gdal.SpatialReference.fromEPSG(4326);
        let coord_transform         = new gdal.CoordinateTransformation( this.dataset.srs, wgs84 ); 
        let corner_names            = Object.keys( this.corners );
        let coordinates_coorners    = [];
        let corners                 = this.corners;
        let geotransform            = this.geotransform;

        corner_names.forEach(function( corner_name ) {
            // convert pixel x,y to the coordinate system of the raster
            // then transform it to WGS84
            let corner      = corners[ corner_name ];
            
            let pt_orig     = {
                x: geotransform[0] + corner.x * geotransform[1] + corner.y * geotransform[2],
                y: geotransform[3] + corner.x * geotransform[4] + corner.y * geotransform[5]
            };

            let pt_wgs84    = coord_transform.transformPoint(pt_orig);
            
            let coordinate = { corner_name : corner_name, Latitude : pt_wgs84.y, Longitude : pt_wgs84.x };
            coordinates_coorners.push( coordinate );
        });

        let bounds =  {};
        coordinates_coorners.map(function( item, index ){

            if( item.corner_name.trim() == "Upper Left" ){
                bounds.north = item.Latitude;
                bounds.west  = item.Longitude; 
            }else if( item.corner_name.trim() == "Bottom Right" ){
                bounds.south = item.Latitude;
                bounds.east = item.Longitude;
            }
        });


        return bounds;
    }
    
}

module.exports = handlerGdal;