const AWS 		= require('aws-sdk');
const fs        = require('fs');
const path      = require('path');

class handlerS3{

    constructor(){
        AWS.config.loadFromPath('./config/awsS3.json');
        
        this.s3Bucket 	= new AWS.S3( { params: {Bucket: 'pro.instacrops', timeout: 6000000} } );    
    }

    downloadFileBucket( filenameS3, callaback ){
        let params      = {
            Key: filenameS3
        };
        
        /* only write on tmp directory */
        let pathfile    = path.resolve( '/tmp/' + filenameS3 ); 
        let filesize    = 0;
        let downloadsize= 0;
        let porcent     = 10;
        let file        = fs.createWriteStream( pathfile );
        
        this.s3Bucket.headObject(params, (error, data) => {
            if(error) console.log( error );

            filesize = data.ContentLength;

            this.s3Bucket.getObject( params ).
            on('error', ( error ) => {
                callaback(error, null);
            }).
            on('httpData', ( chunk ) => {
                file.write( chunk );
                downloadsize += chunk.length;

                let totaldownload = Math.floor( ((downloadsize * 100 ) / filesize) );
                
                if( porcent == totaldownload && porcent < 100){
                    console.log( "======================================");
                    console.log( "Total Download File : " + porcent +"%");
                    console.log( "======================================");
                    porcent += 10;
                }
            }).
            on('httpDone', () => {
                file.end();
                console.log("File Download OK!");
                console.log( "======================================");
                callaback( null, pathfile ); 
            })
            .send();
        }); 
    }

    uploadFileBucket( fileupload, filename, contentType, callaback ){
        let params = {
            ACL: 'public-read',
            Key: filename,
            Body: fileupload,
            ContentType: contentType
        };

       this.s3Bucket.putObject( params).
        on('error', ( error ) => {
            callaback(error, null);
        }).
        on('httpUploadProgress', ( progress ) => {
            console.log("===============================================");
            console.log("Porcentaje Upload " + Math.floor( progress.loaded/progress.total * 100 ) + "%");
            console.log("===============================================");
        }).
        on('success', ( response ) => {
            callaback( null, response );
        }).
        send();
    }

    fileSizeBucket( filenameS3 ){
        let params = { Key : filenameS3 };
        this.s3Bucket.headObject(params, ( error, data ) => {
            if( error ){
                console.log( error );
            }
            else{ return data.ContentLength; }
        });
    }
    
    getPublicUrlObject( filenameS3, callaback){
        let params = { Key : filenameS3, ContentType: 'binary' };
        this.s3Bucket.getSignedUrl('putObject', params, (error, url) => {
            if( error ) callaback(error, null);
            
            callaback(null, url);
        });
    }

}


module.exports = handlerS3;