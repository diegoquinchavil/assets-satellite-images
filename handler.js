'use strict';
/** imports services */
const handlerS3           = require("./services/handlerS3");
const handlerAPISatellite = require('./services/handlerAPISatellite');
const handlerBuildKML     = require("./services/handlerBuildKML");
const handlerGdal         = require('./services/handlerGdal');

/** imports models */
const modelCoordinates    = require('./models/modelCoordinates');

/** imports others packages */
const fs                  = require("fs");
const https               = require("https");

module.exports.start = (event, context, callback) => {

  if( event.body ){
    /** get inputs */
    let body        = JSON.parse(event.body);
    let satellite   = body.satellite;
    let layers      = body.layers;
    let client_id   = body.clientid;
    let objects_sel = body.objects;
    let time        = body.time;

    /** instances classes services */
    const s3  = new handlerS3();
    const api = new handlerAPISatellite();
    const kml = new handlerBuildKML();
    
    /** instances classes models */
    const model_coordinates = new modelCoordinates();

    /** build base URI */
    api.setService( "WMS" );
    api.setRequest( "GetMap&SHOWLOGO=false" );
    api.setVersion( "1.3.0" );
    api.setUrlLayers( layers );
    api.setMaxCC( "24" );
    api.setGain( "1.0" );
    api.setGamma( "1.0" );
    api.setWidth( "600" );
    api.setHeight( "600" );
    api.setCrs( "EPSG:4326" );
    api.setFormat( "image/png" );
    api.setTransparent( "1" );
    api.setTime( time );

    /** call mission area client */
    model_coordinates.getCoordinatesMissionArea( client_id, objects_sel )
    .then( ( results ) => {
      /** if exists mission area */
      if( results ){

        /** init config kml */
        let coordinates_url = "";
        let project_id    = 0;
        let max_longitude = results[0].longitude;
        let min_longitude = results[0].longitude;
        let max_latitude  = results[0].latitude;
        let min_latitude  = results[0].latitude;

        results.map( ( item, index ) => {   

            project_id = item.project_id;//id project 

            coordinates_url = coordinates_url.concat( item.latitude + " " + item.longitude);
          
            if( results.length != ( index + 1) ){
              coordinates_url = coordinates_url.concat(",")
            }

            /** set max longitude */
            if( item.longitude > max_longitude ){
              max_longitude = item.longitude;
            }
            /** set min longitude */
            if( item.longitude < min_longitude ){
              min_longitude = item.longitude;
            }
            /** set max latitude */
            if( item.latitude > max_latitude ){
              max_latitude = item.latitude;
            }
            /** set min latitude */
            if( item.latitude < min_latitude ){
              min_latitude = item.latitude;
            }
        });

        /** add bounds and polygon to URI */
        api.setBounds( max_latitude + "," + max_longitude + "," + min_latitude + "," + min_longitude );
        api.setPolygon( coordinates_url );

        /** print final endpoint from URI*/
        console.log("============================================");
        console.log("Final endpoint => ", api.getEndpointAPI() );
        console.log("============================================");
        
        /** write file return from endpoint */
        var file = fs.createWriteStream("/tmp/polygon.png");
        var request = https.get( api.getEndpointAPI(), function(response) {

            response.pipe(file);
            
            /** finish write file ... */
            response.on("end", () => {
                /** read file */
                fs.readFile("/tmp/polygon.png", function(err, fileData) {
                  if( err ) console.log(err);
                  /** declare url_image s3 */
                  let string_range = time.split("/");
                  let url_image = "img/img_" + client_id + "_polygon_" + satellite + "_" + layers + "_" + string_range[0] +"_"+ string_range[1] + ".png";

                  /** upload image to s3 */
                  s3.uploadFileBucket( fileData, url_image, 'image/png', function( error, result ){
                    if( error ) console.log( error );

                        /** build KML */
                        kml.addGroundOverlay( layers );
                        kml.addGroundOverlayIcon( "https://s3.amazonaws.com/pro.instacrops/" + url_image );
                        kml.addGroundOverlayBox( { north : max_latitude, south : min_latitude, west : min_longitude, east : max_longitude } );
                        
                        let final_kml = kml.getKMLString();

                        /** declare url_kml s3 */
                        let url_kml = "kml/kml_" + client_id + "_polygon_" + satellite + "_" + layers + "_" + string_range[0] +"_"+ string_range[1] + ".kml"; 
                        
                        /** upload KML file to s3*/
                        s3.uploadFileBucket( final_kml, url_kml, 'application/vnd.google-earth.kml+xml', (error, result) => {
                          if(error) callback(error, null);

                          let kml_name = "kml_" + client_id + "_polygon_" + satellite + "_" + layers + "_" + string_range[0] +"_"+ string_range[1];

                          /** insert new kml data to client */
                          model_coordinates.insertKmlProject( project_id, url_kml,  kml_name )
                          .then( ( insert ) => {
                              
                              let res = {     
                                statusCode: 200,
                                headers: {
                                  'Access-Control-Allow-Origin': '*' 
                                },
                                body: JSON.stringify({
                                  success : true, 
                                  message : 'File process OK',
                                  insertId : insert.insertId,
                                  kml_name : kml_name,
                                  kml_url : url_kml
                                })
                              };
                            
                              callback( null, res );
                          });
                          
                        });
                    });
              });            
            });//end download satellite image 
        });
        
      } //end exist result
    });

  }else{
    let res = {     
      statusCode: 100,
      body: JSON.stringify({
        message: 'Invalid parameters!'
      }),
    };
  
    callback( null, res );
  }
};
