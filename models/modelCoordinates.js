const mysql = require("mysql");
const db    = require( __dirname + '/../config/database.json')[ 'production' ];

class modelCoordinates{
    constructor(){
        this.id         = null;
        this.latitude   = 0;
        this.longitude  = 0;
        this.object_id  = 0;

    }

    getConexion(){
        this.connection = mysql.createConnection({
            host     : db.host,
            user     : db.username,
            password : db.password,
            database : db.database
        }); 
    }

    getCoordinatesMissionArea( _clientid, _objects ){
        return new Promise( ( success , fails ) => {
            this.getConexion();
            this.connection.connect();

            let query = 'SELECT p.id as project_id, c.latitude, c.longitude ' +
                        'FROM maps_project p ' +
                        'INNER JOIN maps_drawing_object o ON o.maps_project_id = p.id ' +
                        'INNER JOIN maps_coordinates c ON c.map_drawing_object_id = o.id ' +
                        'WHERE p.client_id = ' + _clientid + ' ' +
                        'AND o.id IN ('+ _objects +')';

            this.connection.query( query , ( error, results, fields ) => {
                if( error ) return fails( error ); 
                
                success( results );
            });

            this.connection.end();
        });
    }

    insertKmlProject( _project_id, _s3name, _filename ){

        return new Promise( ( success, fails )  => {
            
            if( this.connection == null || this.connection.state === 'disconnected' ){
                this.getConexion();
                this.connection.connect();
            }
            
            let ts      = new Date( );
            let query   = "INSERT INTO maps_kml(filename, s3name, ts, description, visible, maps_project_id, maps_category_id)" +
                           "VALUES('"+ _filename +"', '" + _s3name + "', now(), 'KML Satellite', 1, "+_project_id + ", 1)";

            this.connection.query(query, ( error, results, fields ) => {
                if( error ) fails( error );

                success( results );
            });

            this.connection.end();
        });
    }

}


module.exports = modelCoordinates;